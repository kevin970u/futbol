from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('listadoPosiciones/', views.listadoPosicion, name='listadoPosiciones'),
    path('listadoEquipo/', views.listadoEquipo, name='listadoEquipo'),
    path('crearPosicion/', views.crearPosicion, name='crearPosicion'),
    path('editarPosicion/<int:id>/', views.editarPosicion, name='editarPosicion'),
    path('eliminarPosicion/<int:id>/', views.eliminarPosicion, name='eliminarPosicion'),
    path('crearEquipo/', views.crearEquipo, name='crearEquipo'),
    path('editarEquipo/<int:id>/', views.editarEquipo, name='editarEquipo'),
    path('eliminarEquipo/<int:id>/', views.eliminarEquipo, name='eliminarEquipo'),
    path('listadoJugador/', views.listadoJugador, name='listadoJugador'),
    path('crearJugador/', views.crearJugador, name='crearJugador'),
    path('editarJugador/<int:id>/', views.editarJugador, name='editarJugador'),
    path('eliminarJugador/<int:id>/', views.eliminarJugador, name='eliminarJugador')
]
