from django.apps import AppConfig


class TrabajpConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.Trabajp'
