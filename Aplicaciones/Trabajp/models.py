from django.db import models

# Create your models here.

from django.db import models

class Posicion(models.Model):
    nombre_pos = models.CharField(max_length=150)
    descripcion_pos = models.TextField()

    def _str_(self):
        fila="{0}: {1}"
        return fila.format(self.nombre_pos,self.descripcion_pos)

class Equipo(models.Model):
    nombre_equi = models.CharField(max_length=500)
    siglas_equi = models.CharField(max_length=25)
    fundacion_equi = models.IntegerField()
    region_equi = models.CharField(max_length=25)
    numero_titulos_equi = models.IntegerField()

    def _str_(self):
        fila="{0}: {1}"
        return fila.format(self.nombre_equi,self.siglas_equi)

class Jugador(models.Model):
    apellido_jug = models.CharField(max_length=500)
    nombre_jug = models.CharField(max_length=500)
    estatura_jug = models.FloatField()
    salario_jug = models.DecimalField(max_digits=10, decimal_places=2)
    estado_jug = models.CharField(max_length=25)
    fk_id_pos = models.ForeignKey(Posicion, on_delete=models.CASCADE)
    fk_id_equi = models.ForeignKey(Equipo, on_delete=models.CASCADE)

    def _str_(self):
        fila="{0}: {1}"
        return fila.format(self.nombre_jug,self.apellido_jug)