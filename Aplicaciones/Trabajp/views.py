# mi_app/views.py
from django.shortcuts import render, redirect
from .models import Posicion, Equipo, Jugador

# PAGINA PRINCIPAL
def inicio(request):
    return render(request, 'home.html')

# FUNCIONES PARA VER LISTADOS
def listadoPosicion(request):
    posicionesBdd = Posicion.objects.all()
    mensaje = request.GET.get('mensaje', '')
    return render(request, "listadoPosiciones.html", {'posiciones': posicionesBdd, 'mensaje': mensaje})

def listadoEquipo(request):
    equiposBdd = Equipo.objects.all()
    mensaje = request.GET.get('mensaje', '')
    return render(request, "listadoEquipo.html", {'equipos': equiposBdd, 'mensaje': mensaje})

def listadoJugador(request):
    jugadores = Jugador.objects.all()
    posiciones = Posicion.objects.all()
    equipos = Equipo.objects.all()
    mensaje = request.GET.get('mensaje', '')
    return render(request, 'listadoJugador.html', {'jugadores': jugadores, 'posiciones': posiciones, 'equipos': equipos, 'mensaje': mensaje})

# FUNCIONES PARA GUARDAR
def guardarPosicion(request):
    if request.method == 'POST':
        nombre_pos = request.POST["nombre_pos"]
        descripcion_pos = request.POST["descripcion_pos"]

        Posicion.objects.create(nombre_pos=nombre_pos, descripcion_pos=descripcion_pos)
        return redirect('/listadoPosiciones/?mensaje=guardado')

    return render(request, 'listadoPosicion.html')

def guardarEquipo(request):
    if request.method == 'POST':
        nombre_equi = request.POST["nombre_equi"]
        siglas_equi = request.POST["siglas_equi"]
        fundacion_equi = request.POST["fundacion_equi"]
        region_equi = request.POST["region_equi"]
        numero_titulos_equi = request.POST["numero_titulos_equi"]

        Equipo.objects.create(nombre_equi=nombre_equi, siglas_equi=siglas_equi, fundacion_equi=fundacion_equi, region_equi=region_equi, numero_titulos_equi=numero_titulos_equi)
        return redirect('/listadoEquipo/?mensaje=guardado')

    return render(request, 'listadoEquipo.html')

def guardarJugador(request):
    if request.method == 'POST':
        nombre_jug = request.POST['nombre_jug']
        apellido_jug = request.POST['apellido_jug']
        estatura_jug = request.POST['estatura_jug']
        salario_jug = request.POST['salario_jug']
        estado_jug = request.POST['estado_jug']
        fk_id_pos = Posicion.objects.get(pk=request.POST['fk_id_pos'])
        fk_id_equi = Equipo.objects.get(pk=request.POST['fk_id_equi'])

        Jugador.objects.create(
            nombre_jug=nombre_jug,
            apellido_jug=apellido_jug,
            estatura_jug=estatura_jug,
            salario_jug=salario_jug,
            estado_jug=estado_jug,
            fk_id_pos=fk_id_pos,
            fk_id_equi=fk_id_equi
        )

        return redirect('/listadoJugador/?mensaje=guardado')

    posiciones = Posicion.objects.all()
    equipos = Equipo.objects.all()
    return render(request, 'crearJugador.html', {'posiciones': posiciones, 'equipos': equipos})

# FUNCIONES PARA EDITAR
def editarPosicion(request, id):
    try:
        posicion = Posicion.objects.get(pk=id)
    except Posicion.DoesNotExist:
        return redirect('/')

    if request.method == 'POST':
        posicion.nombre_pos = request.POST["nombre_pos"]
        posicion.descripcion_pos = request.POST["descripcion_pos"]
        posicion.save()
        return redirect('/listadoPosiciones/?mensaje=editado')

    return render(request, 'editarPosicion.html', {'posicion': posicion})

def editarEquipo(request, id):
    try:
        equipo = Equipo.objects.get(pk=id)
    except Equipo.DoesNotExist:
        return redirect('/')

    if request.method == 'POST':
        equipo.nombre_equi = request.POST["nombre_equi"]
        equipo.siglas_equi = request.POST["siglas_equi"]
        equipo.fundacion_equi = request.POST["fundacion_equi"]
        equipo.region_equi = request.POST["region_equi"]
        equipo.numero_titulos_equi = request.POST["numero_titulos_equi"]
        equipo.save()
        return redirect('/listadoEquipo/?mensaje=editado')

    return render(request, 'editarEquipo.html', {'equipo': equipo})

def editarJugador(request, id):
    try:
        jugador = Jugador.objects.get(pk=id)
    except Jugador.DoesNotExist:
        return redirect('/listadoJugador')

    if request.method == 'POST':
        jugador.nombre_jug = request.POST['nombre_jug']
        jugador.apellido_jug = request.POST['apellido_jug']
        jugador.estatura_jug = request.POST['estatura_jug']
        jugador.salario_jug = request.POST['salario_jug']
        jugador.estado_jug = request.POST['estado_jug']
        jugador.fk_id_pos = Posicion.objects.get(pk=request.POST['fk_id_pos'])
        jugador.fk_id_equi = Equipo.objects.get(pk=request.POST['fk_id_equi'])
        jugador.save()

        return redirect('/listadoJugador/?mensaje=editado')

    posiciones = Posicion.objects.all()
    equipos = Equipo.objects.all()
    return render(request, 'editarJugador.html', {'jugador': jugador, 'posiciones': posiciones, 'equipos': equipos})

# FUNCIONES PARA CREAR
def crearPosicion(request):
    if request.method == 'POST':
        nombre_pos = request.POST["nombre_pos"]
        descripcion_pos = request.POST["descripcion_pos"]

        Posicion.objects.create(nombre_pos=nombre_pos, descripcion_pos=descripcion_pos)
        return redirect('/listadoPosiciones/?mensaje=guardado')

    return render(request, 'crearPosicion.html')

def crearEquipo(request):
    if request.method == 'POST':
        nombre_equi = request.POST["nombre_equi"]
        siglas_equi = request.POST["siglas_equi"]
        fundacion_equi = request.POST["fundacion_equi"]
        region_equi = request.POST["region_equi"]
        numero_titulos_equi = request.POST["numero_titulos_equi"]

        Equipo.objects.create(nombre_equi=nombre_equi, siglas_equi=siglas_equi, fundacion_equi=fundacion_equi, region_equi=region_equi, numero_titulos_equi=numero_titulos_equi)
        return redirect('/listadoEquipo/?mensaje=guardado')

    return render(request, 'crearEquipo.html')

def crearJugador(request):
    if request.method == 'POST':
        nombre_jug = request.POST['nombre_jug']
        apellido_jug = request.POST['apellido_jug']
        estatura_jug = request.POST['estatura_jug']
        salario_jug = request.POST['salario_jug']
        estado_jug = request.POST['estado_jug']
        fk_id_pos = Posicion.objects.get(pk=request.POST['fk_id_pos'])
        fk_id_equi = Equipo.objects.get(pk=request.POST['fk_id_equi'])

        Jugador.objects.create(
            nombre_jug=nombre_jug,
            apellido_jug=apellido_jug,
            estatura_jug=estatura_jug,
            salario_jug=salario_jug,
            estado_jug=estado_jug,
            fk_id_pos=fk_id_pos,
            fk_id_equi=fk_id_equi
        )

        return redirect('/listadoJugador/?mensaje=guardado')

    posiciones = Posicion.objects.all()
    equipos = Equipo.objects.all()
    return render(request, 'crearJugador.html', {'posiciones': posiciones, 'equipos': equipos})

# FUNCIONES PARA ELIMINAR
def eliminarPosicion(request, id):
    try:
        posicion = Posicion.objects.get(pk=id)
        posicion.delete()
        return redirect('/listadoPosiciones/?mensaje=eliminado')
    except Posicion.DoesNotExist:
        pass
    return redirect('/')

def eliminarEquipo(request, id):
    try:
        equipo = Equipo.objects.get(pk=id)
        equipo.delete()
        return redirect('/listadoEquipo/?mensaje=eliminado')
    except Equipo.DoesNotExist:
        pass
    return redirect('/')

def eliminarJugador(request, id):
    try:
        jugador = Jugador.objects.get(pk=id)
        jugador.delete()
        return redirect('/listadoJugador/?mensaje=eliminado')
    except Jugador.DoesNotExist:
        pass
    
    return redirect('/listadoJugador')
