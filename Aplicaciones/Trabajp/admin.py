from django.contrib import admin
from .models import Posicion
from .models import Equipo
from .models import Jugador


# Register your models here.
admin.site.register(Posicion)
admin.site.register(Equipo)
admin.site.register(Jugador)

